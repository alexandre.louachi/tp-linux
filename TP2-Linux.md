PARTIE 1:


🌞 Installer MariaDB sur la machine db.tp2.cesi

```bash
[root@LAPTOP-82OKAHT9 ~]#yum install mariadb-server
Last metadata expiration check: 0:08:53 ago on Tue 07 Dec 2021 02:50:31 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                           Architecture  Version                                         Repository        Size
========================================================================================================================
Installing:
 mariadb-server                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream         16 M
Installing dependencies:
 mariadb                           x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream        6.0 M
 mariadb-common                    x86_64        3:10.3.28-1.module+el8.4.0+427+adf35707         appstream         62 k
 mariadb-connector-c               x86_64        3.1.11-2.el8_3                                  appstream        199 k
 mariadb-connector-c-config        noarch        3.1.11-2.el8_3                                  appstream         14 k
```






🌞 Le service MariaDB

```bash
[root@LAPTOP-82OKAHT9 ~]# systemctl start mariadb
[root@LAPTOP-82OKAHT9 ~]# systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:00:45 CET; 9s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 26942 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 26807 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 26783 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 26910 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4943)
   Memory: 83.4M
   CGroup: /system.slice/mariadb.service
           └─26910 /usr/libexec/mysqld --basedir=/usr

Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: See the MariaDB Knowledgebase at http://mariadb.com/k>
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: MySQL manual for more instructions.
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: Please report any problems at http://mariadb.org/jira
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: The latest information about MariaDB is available at >
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: You can find additional information about the MySQL p>
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: http://dev.mysql.com
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: Consider joining MariaDB's strong and vibrant communi>
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysql-prepare-db-dir[26807]: https://mariadb.org/get-involved/
Dec 07 15:00:45 LAPTOP-82OKAHT9.home mysqld[26910]: 2021-12-07 15:00:45 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-Ma>
Dec 07 15:00:45 LAPTOP-82OKAHT9.home systemd[1]: Started MariaDB 10.3 database server.
lines 1-25/25 (END)
```








```bash
[root@LAPTOP-82OKAHT9 ~]#sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```








```bash
[root@LAPTOP-82OKAHT9 ~]# sudo ss -alnpt
State     Recv-Q    Send-Q         Local Address:Port         Peer Address:Port    Process
LISTEN    0         128                  0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=852,fd=5))
LISTEN    0         80                         *:3306                    *:*        users:(("mysqld",pid=936,fd=21))
LISTEN    0         128                     [::]:22                   [::]:*        users:(("sshd",pid=852,fd=7))
```







```bash
[root@LAPTOP-82OKAHT9 ~]# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 14:12 ?        00:00:03 /usr/lib/systemd/systemd --switched-root --system --deserialize 16
root           2       0  0 14:12 ?        00:00:00 [kthreadd]
root           3       2  0 14:12 ?        00:00:00 [rcu_gp]
root           4       2  0 14:12 ?        00:00:00 [rcu_par_gp]
root           6       2  0 14:12 ?        00:00:00 [kworker/0:0H-events_highpri]
root           9       2  0 14:12 ?        00:00:00 [mm_percpu_wq]
root          10       2  0 14:12 ?        00:00:00 [ksoftirqd/0]
root          11       2  0 14:12 ?        00:00:00 [rcu_sched]
root          12       2  0 14:12 ?        00:00:00 [migration/0]
root          13       2  0 14:12 ?        00:00:00 [watchdog/0]
root          14       2  0 14:12 ?        00:00:00 [cpuhp/0]
root          16       2  0 14:12 ?        00:00:00 [kdevtmpfs]
root          17       2  0 14:12 ?        00:00:00 [netns]
root          18       2  0 14:12 ?        00:00:00 [rcu_tasks_trace]
root          19       2  0 14:12 ?        00:00:00 [rcu_tasks_rude_]
root          20       2  0 14:12 ?        00:00:00 [kauditd]
root          21       2  0 14:12 ?        00:00:00 [khungtaskd]
root          22       2  0 14:12 ?        00:00:00 [oom_reaper]
root          23       2  0 14:12 ?        00:00:00 [writeback]
root          24       2  0 14:12 ?        00:00:00 [kcompactd0]
root          25       2  0 14:12 ?        00:00:00 [ksmd]
root          26       2  0 14:12 ?        00:00:00 [khugepaged]
root          27       2  0 14:12 ?        00:00:00 [crypto]
```






🌞 Firewall

```bash
[root@LAPTOP-82OKAHT9 ~]# sudo firewall-cmd --add-port=3306/tcp --permanent
success
```





🌞 Configuration élémentaire de la base

```bash
[root@LAPTOP-82OKAHT9 ~]# mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] n
 ... skipping.

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```









🌞 Préparation de la base en vue de l'utilisation par NextCloud


```bash
[root@LAPTOP-82OKAHT9 ~]# sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 22
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> REATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'REATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_...' at line 1
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```




🌞 Installez sur la machine web.tp2.cesi la commande mysql



```bash
[root@localhost ~]# dnf provides mysql
Last metadata expiration check: 0:01:43 ago on Tue 07 Dec 2021 03:27:36 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```




🌞 Tester la connexion

```bash
[root@localhost ~]# mysql -h 10.2.1.12 -u nextcloud -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 30
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.01 sec)
```




🌞 Installer Apache sur la machine web.tp2.cesi

```bash
[root@localhost ~]# yum install httpd
Last metadata expiration check: 0:30:49 ago on Tue 07 Dec 2021 03:27:36 PM CET.
Dependencies resolved.
=============================================================================================
 Package              Arch      Version                                   Repository    Size
=============================================================================================
Installing:
 httpd                x86_64    2.4.37-43.module+el8.5.0+714+5ec56ee8     appstream    1.4 M
Installing dependencies:
 apr                  x86_64    1.6.3-12.el8                              appstream    128 k
 apr-util             x86_64    1.6.1-6.el8.1                             appstream    104 k
 httpd-filesystem     noarch    2.4.37-43.module+el8.5.0+714+5ec56ee8     appstream     38 k
 httpd-tools          x86_64    2.4.37-43.module+el8.5.0+714+5ec56ee8     appstream    106 k
 mod_http2            x86_64    1.15.7-3.module+el8.5.0+695+1fa8055e      appstream    153 k
 rocky-logos-httpd    noarch    85.0-3.el8
```




🌞 Analyse du service Apache

```bash
[root@localhost ~]# systemctl start httpd
[root@localhost ~]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:59:05 CET; 5s ago
     Docs: man:httpd.service(8)
 Main PID: 26229 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4943)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─26229 /usr/sbin/httpd -DFOREGROUND
           ├─26230 /usr/sbin/httpd -DFOREGROUND
           ├─26231 /usr/sbin/httpd -DFOREGROUND
           ├─26232 /usr/sbin/httpd -DFOREGROUND
           └─26233 /usr/sbin/httpd -DFOREGROUND

Dec 07 15:59:05 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 15:59:05 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 15:59:05 web.tp2.cesi httpd[26229]: Server configured, listening on: port 80
```






```bash
[root@localhost ~]# sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.


ss
Netid  State  Recv-Q  Send-Q                                           Local Address:Port                           Peer Address:Port                        Process
u_str  ESTAB  0       0                                                            * 54134                                     * 0
u_str  ESTAB  0       0                                  /run/systemd/journal/stdout 18982                                     * 18981
u_str  ESTAB  0       0                                                            * 20729                                     * 20730
u_str  ESTAB  0       0                                                            * 54120                                     * 0
u_str  ESTAB  0       0                                                            * 23428                                     * 23427
u_str  ESTAB  0       0                      /var/lib/sss/pipes/private/sbus-monitor 20973                                     * 20972
u_str  ESTAB  0       0                                  /run/dbus/system_bus_socket 22213                                     * 22212
u_str  ESTAB  0       0                                                            * 20732                                     * 20733
u_str  ESTAB  0       0                                  /run/systemd/journal/stdout 24930                                     * 24928
u_str  ESTAB  0       0                                                            * 21515                                     * 0
u_str  ESTAB  0       0                                  /run/systemd/journal/stdout 20390                                     * 20389
u_str  ESTAB  0       0        /var/lib/sss/pipes/private/sbus-dp_implicit_files.811 20965                                     * 20964
u_str  ESTAB  0       0                                                            * 21108                                     * 21109
```






```bash
[root@localhost ~]# ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
root           1       0  0 14:25 ?        00:00:02 /usr/lib/systemd/systemd --switched-root
root           2       0  0 14:25 ?        00:00:00 [kthreadd]
root           3       2  0 14:25 ?        00:00:00 [rcu_gp]
root           4       2  0 14:25 ?        00:00:00 [rcu_par_gp]
root           6       2  0 14:25 ?        00:00:00 [kworker/0:0H-events_highpri]
root           9       2  0 14:25 ?        00:00:00 [mm_percpu_wq]
root          10       2  0 14:25 ?        00:00:00 [ksoftirqd/0]
root          11       2  0 14:25 ?        00:00:00 [rcu_sched]
root          12       2  0 14:25 ?        00:00:00 [migration/0]
root          13       2  0 14:25 ?        00:00:00 [watchdog/0]
root          14       2  0 14:25 ?        00:00:00 [cpuhp/0]
root          16       2  0 14:25 ?        00:00:00 [kdevtmpfs]
root          17       2  0 14:25 ?        00:00:00 [netns]
root          18       2  0 14:25 ?        00:00:00 [rcu_tasks_trace]
root          19       2  0 14:25 ?        00:00:00 [rcu_tasks_rude_]
root          20       2  0 14:25 ?        00:00:00 [kauditd]
root          21       2  0 14:25 ?        00:00:00 [khungtaskd]
root          22       2  0 14:25 ?        00:00:00 [oom_reaper]
root          23       2  0 14:25 ?        00:00:00 [writeback]
root          24       2  0 14:25 ?        00:00:00 [kcompactd0]
root          25       2  0 14:25 ?        00:00:00 [ksmd]
root          26       2  0 14:25 ?        00:00:00 [khugepaged]
root          27       2  0 14:25 ?        00:00:00 [crypto]
root          28       2  0 14:25 ?        00:00:00 [kintegrityd]
```



```bash
[root@localhost ~]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 15:59:05 CET; 10min ago
     Docs: man:httpd.service(8)
 Main PID: 26229 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4943)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─26229 /usr/sbin/httpd -DFOREGROUND
           ├─26230 /usr/sbin/httpd -DFOREGROUND
           ├─26231 /usr/sbin/httpd -DFOREGROUND
           ├─26232 /usr/sbin/httpd -DFOREGROUND
           └─26233 /usr/sbin/httpd -DFOREGROUND

Dec 07 15:59:05 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 15:59:05 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 15:59:05 web.tp2.cesi httpd[26229]: Server configured, listening on: port 80
```

OU sudo ss -alnpt




🌞 Un premier test

```bash
[root@localhost ~]# sudo firewall-cmd --add-port=80/tcp --permanent
success
[root@localhost ~]# sudo firewall-cmd --reload
success

[root@localhost ~]# curl 10.2.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }


  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;



```

🌞 Installer PHP


```bash
[root@localhost ~]# sudo dnf install epel-release
Last metadata expiration check: 0:46:31 ago on Tue 07 Dec 2021 03:27:36 PM CET.
Dependencies resolved.
============================================================================================
 Package                  Architecture       Version               Repository          Size
============================================================================================
Installing:
 epel-release             noarch             8-13.el8              extras              23 k

Transaction Summary
============================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                             75 kB/s |  23 kB     00:00
--------------------------------------------------------------------------------------------
Total                                                        39 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                    1/1
  Installing       : epel-release-8-13.el8.noarch                                       1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                       1/1
  Verifying        : epel-release-8-13.el8.noarch                                       1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```


```bash
[root@localhost ~]# sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64              2.4 MB/s |  11 MB     00:04
Extra Packages for Enterprise Linux Modular 8 - x86_64      1.1 MB/s | 980 kB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 07 Dec 2021 04:14:49 PM CET.
Dependencies resolved.
============================================================================================
 Package               Arch      Version                                 Repository    Size
============================================================================================
Installing:
 kernel                x86_64    4.18.0-348.2.1.el8_5                    baseos       7.0 M
Upgrading:
 binutils              x86_64    2.30-108.el8_5.1                        baseos       5.8 M
 bpftool               x86_64    4.18.0-348.2.1.el8_5                    baseos       7.7 M
 kernel-tools          x86_64    4.18.0-348.2.1.el8_5                    baseos       7.2 M
 kernel-tools-libs     x86_64    4.18.0-348.2.1.el8_5                    baseos       7.0 M
 libgcc                x86_64    8.5.0-4.el8_5                           baseos        78 k
 libgomp               x86_64    8.5.0-4.el8_5                           baseos       205 k
 libipa_hbac           x86_64    2.5.2-2.el8_5.1                         baseos       114 k
 libsss_autofs         x86_64    2.5.2-2.el8_5.1                         baseos       116 k
 libsss_certmap        x86_64    2.5.2-2.el8_5.1                         baseos       154 k
 libsss_idmap          x86_64    2.5.2-2.el8_5.1                         baseos       119 k
 libsss_nss_idmap      x86_64    2.5.2-2.el8_5.1                         baseos       125 k
 libsss_sudo           x86_64    2.5.2-2.el8_5.1                         baseos       115 k
 libstdc++             x86_64    8.5.0-4.el8_5                           baseos       452 k
 perl-DBD-SQLite       x86_64    1.58-2.module+el8.4.0+515+5c88ffe5      appstream    192 k
 perl-DBI              x86_64    1.641-3.module+el8.4.0+509+59a8d9b3     appstream    739 k
 python3-perf          x86_64    4.18.0-348.2.1.el8_5                    baseos       7.1 M
 python3-sssdconfig    noarch    2.5.2-2.el8_5.1                         baseos       141 k
 sssd                  x86_64    2.5.2-2.el8_5.1                         baseos       105 k
 sssd-ad               x86_64    2.5.2-2.el8_5.1                         baseos       269 k
 sssd-client           x86_64    2.5.2-2.el8_5.1                         baseos       204 k
 sssd-common           x86_64    2.5.2-2.el8_5.1                         baseos       1.6 M
 sssd-common-pac       x86_64    2.5.2-2.el8_5.1                         baseos       177 k
 sssd-ipa              x86_64    2.5.2-2.el8_5.1                         baseos       346 k
 sssd-kcm              x86_64    2.5.2-2.el8_5.1                         baseos       253 k
 sssd-krb5             x86_64    2.5.2-2.el8_5.1                         baseos       148 k
 sssd-krb5-common      x86_64    2.5.2-2.el8_5.1                         baseos       184 k
 sssd-ldap             x86_64    2.5.2-2.el8_5.1                         baseos       207 k
 sssd-nfs-idmap        x86_64    2.5.2-2.el8_5.1                         baseos       114 k
 sssd-proxy            x86_64    2.5.2-2.el8_5.1                         baseos       146 k
 unzip                 x86_64    6.0-45.el8_4                            baseos       194 k
Installing dependencies:
 bind-libs             x86_64    32:9.11.26-6.el8                        appstream    173 k
 bind-libs-lite        x86_64    32:9.11.26-6.el8                        appstream    1.2 M
 bind-license          noarch    32:9.11.26-6.el8                        appstream    101 k
 fstrm                 x86_64    0.6.1-2.el8                             appstream     28 k
 kernel-core           x86_64    4.18.0-348.2.1.el8_5                    baseos        38 M
 kernel-modules        x86_64    4.18.0-348.2.1.el8_5                    baseos        30 M
 libmaxminddb          x86_64    1.2.0-10.el8                            appstream     32 k
 protobuf-c            x86_64    1.3.0-6.el8                             appstream     36 k
 python3-bind          noarch    32:9.11.26-6.el8                        appstream    149 k
 python3-ply           noarch    3.9-9.el8                               baseos       110 k
Installing weak dependencies:
 bind-utils            x86_64    32:9.11.26-6.el8                        appstream    450 k
 geolite2-city         noarch    20180605-1.el8                          appstream     19 M
 geolite2-country      noarch    20180605-1.el8                          appstream    1.0 M
Enabling module streams:
 perl                            5.26
 perl-DBD-SQLite                 1.58
 perl-DBI                        1.641

Transaction Summary
============================================================================================
Install  14 Packages
Upgrade  30 Packages

Total download size: 138 M
Is this ok [y/N]: y
Downloading Packages:
(1/44): bind-license-9.11.26-6.el8.noarch.rpm               284 kB/s | 101 kB     00:00
(2/44): bind-utils-9.11.26-6.el8.x86_64.rpm                 1.7 MB/s | 450 kB     00:00
(3/44): fstrm-0.6.1-2.el8.x86_64.rpm                        441 kB/s |  28 kB     00:00
(4/44): bind-libs-9.11.26-6.el8.x86_64.rpm                   57 kB/s | 173 kB     00:03
(5/44): geolite2-country-20180605-1.el8.noarch.rpm          165 kB/s | 1.0 MB     00:06
(6/44): libmaxminddb-1.2.0-10.el8.x86_64.rpm                151 kB/s |  32 kB     00:00
(7/44): protobuf-c-1.3.0-6.el8.x86_64.rpm                   135 kB/s |  36 kB     00:00
(8/44): python3-bind-9.11.26-6.el8.noarch.rpm               376 kB/s | 149 kB     00:00
(9/44): geolite2-city-20180605-1.el8.noarch.rpm             1.9 MB/s |  19 MB     00:09
(10/44): bind-libs-lite-9.11.26-6.el8.x86_64.rpm             79 kB/s | 1.2 MB     00:15
(11/44): kernel-4.18.0-348.2.1.el8_5.x86_64.rpm             1.3 MB/s | 7.0 MB     00:05
(12/44): python3-ply-3.9-9.el8.noarch.rpm                   1.4 MB/s | 110 kB     00:00
(13/44): perl-DBD-SQLite-1.58-2.module+el8.4.0+515+5c88ffe5 256 kB/s | 192 kB     00:00
(14/44): perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_6 301 kB/s | 739 kB     00:02
(15/44): binutils-2.30-108.el8_5.1.x86_64.rpm               552 kB/s | 5.8 MB     00:10
(16/44): bpftool-4.18.0-348.2.1.el8_5.x86_64.rpm            934 kB/s | 7.7 MB     00:08
(17/44): kernel-tools-4.18.0-348.2.1.el8_5.x86_64.rpm       1.1 MB/s | 7.2 MB     00:06
(18/44): kernel-core-4.18.0-348.2.1.el8_5.x86_64.rpm        1.0 MB/s |  38 MB     00:36
(19/44): libgcc-8.5.0-4.el8_5.x86_64.rpm                    196 kB/s |  78 kB     00:00
(20/44): libgomp-8.5.0-4.el8_5.x86_64.rpm                   532 kB/s | 205 kB     00:00
(21/44): libipa_hbac-2.5.2-2.el8_5.1.x86_64.rpm             263 kB/s | 114 kB     00:00
(22/44): libsss_autofs-2.5.2-2.el8_5.1.x86_64.rpm           308 kB/s | 116 kB     00:00
(23/44): kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64.rpm  1.7 MB/s | 7.0 MB     00:04
(24/44): libsss_certmap-2.5.2-2.el8_5.1.x86_64.rpm          621 kB/s | 154 kB     00:00
(25/44): libsss_idmap-2.5.2-2.el8_5.1.x86_64.rpm            747 kB/s | 119 kB     00:00
(26/44): libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64.rpm        779 kB/s | 125 kB     00:00
(27/44): libsss_sudo-2.5.2-2.el8_5.1.x86_64.rpm             710 kB/s | 115 kB     00:00
```







```bash
[root@localhost ~]# sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rmm
Last metadata expiration check: 0:03:26 ago on Tue 07 Dec 2021 04:14:49 PM CET.
remi-release-8.rpm                                          134 kB/s |  26 kB     00:00
Dependencies resolved.
============================================================================================
 Package               Architecture    Version                  Repository             Size
============================================================================================
Installing:
 remi-release          noarch          8.5-2.el8.remi           @commandline           26 k

Transaction Summary
============================================================================================
Install  1 Package

Total size: 26 k
Installed size: 21 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                    1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                 1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                 1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

```




```bash
[root@localhost ~]# dnf module enable php:remi-7.4

Remi's Modular repository for Enterprise Linux 8 - x86_64   3.0 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64   3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: Remi's Modular repository      [   ===                    ] ---  B/s |   0Remi's Modular repository for Enterprise Linux 8 - x86_64   7.8 kB/s | 858  B     00:00
Error: Failed to download metadata for repo 'remi-modular': repomd.xml GPG signature verification error: Bad GPG signature
```

```bash
[root@localhost ~]# dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64   3.6 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64   3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64   1.5 MB/s | 946 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64  4.9 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64  3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64  1.9 MB/s | 2.0 MB     00:01
Last metadata expiration check: 0:00:02 ago on Tue 07 Dec 2021 04:19:02 PM CET.
Dependencies resolved.
============================================================================================
 Package              Architecture        Version                Repository            Size
============================================================================================
Enabling module streams:
 php                                      remi-7.4

Transaction Summary
============================================================================================

Is this ok [y/N]: y
Complete!
```






```bash
[root@localhost ~]# sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:01:05 ago on Tue 07 Dec 2021 04:19:02 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
============================================================================================
 Package                       Arch    Version                             Repository  Size
============================================================================================
Installing:
 php74-php                     x86_64  7.4.26-1.el8.remi                   remi-safe  1.5 M
 php74-php-bcmath              x86_64  7.4.26-1.el8.remi                   remi-safe   88 k
 php74-php-common              x86_64  7.4.26-1.el8.remi                   remi-safe  710 k
 php74-php-gd                  x86_64  7.4.26-1.el8.remi                   remi-safe   93 k
 php74-php-gmp                 x86_64  7.4.26-1.el8.remi                   remi-safe   84 k
 php74-php-intl                x86_64  7.4.26-1.el8.remi                   remi-safe  201 k
 php74-php-json                x86_64  7.4.26-1.el8.remi                   remi-safe   82 k
 php74-php-mbstring            x86_64  7.4.26-1.el8.remi                   remi-safe  492 k
 php74-php-mysqlnd             x86_64  7.4.26-1.el8.remi                   remi-safe  200 k
 php74-php-pdo                 x86_64  7.4.26-1.el8.remi                   remi-safe  130 k
 php74-php-pecl-zip            x86_64  1.20.0-1.el8.remi                   remi-safe   58 k
 php74-php-process             x86_64  7.4.26-1.el8.remi                   remi-safe   92 k
 php74-php-xml                 x86_64  7.4.26-1.el8.remi                   remi-safe  180 k
Installing dependencies:
 checkpolicy                   x86_64  2.9-1.el8                           baseos     345 k
 environment-modules           x86_64  4.5.2-1.el8                         baseos     420 k
 fontconfig                    x86_64  2.13.1-4.el8                        baseos     273 k
 gd                            x86_64  2.2.5-7.el8                         appstream  143 k
 jbigkit-libs                  x86_64  2.1-14.el8                          appstream   54 k
 libX11                        x86_64  1.6.8-5.el8                         appstream  610 k
 libX11-common                 noarch  1.6.8-5.el8                         appstream  157 k
 libXau                        x86_64  1.0.9-3.el8                         appstream   36 k
 libXpm                        x86_64  3.5.12-8.el8                        appstream   57 k
 libicu69                      x86_64  69.1-1.el8.remi                     remi-safe  9.6 M
 libjpeg-turbo                 x86_64  1.5.3-12.el8                        appstream  156 k
 libsodium                     x86_64  1.0.18-2.el8                        epel       162 k
 libtiff                       x86_64  4.0.9-20.el8                        appstream  187 k
 libwebp                       x86_64  1.0.0-5.el8                         appstream  271 k
 libxcb                        x86_64  1.13.1-1.el8                        appstream  228 k
 libxslt                       x86_64  1.1.32-6.el8                        baseos     249 k
 oniguruma5php                 x86_64  6.9.7.1-1.el8.remi                  remi-safe  210 k
 php74-libzip                  x86_64  1.8.0-1.el8.remi                    remi-safe   69 k
 php74-runtime                 x86_64  1.0-3.el8.remi                      remi-safe  1.1 M
 policycoreutils-python-utils  noarch  2.9-16.el8                          baseos     251 k
 python3-audit                 x86_64  3.0-0.17.20191104git1c2f876.el8.1   baseos      85 k
 python3-libsemanage           x86_64  2.9-6.el8                           baseos     126 k
 python3-policycoreutils       noarch  2.9-16.el8                          baseos     2.2 M
 python3-setools               x86_64  4.3.0-2.el8                         baseos     625 k
 scl-utils                     x86_64  1:2.0.2-14.el8                      appstream   46 k
 tcl                           x86_64  1:8.6.8-2.el8                       baseos     1.1 M
Installing weak dependencies:
 php74-php-cli                 x86_64  7.4.26-1.el8.remi                   remi-safe  3.1 M
 php74-php-fpm                 x86_64  7.4.26-1.el8.remi                   remi-safe  1.6 M
 php74-php-opcache             x86_64  7.4.26-1.el8.remi                   remi-safe  275 k
 php74-php-sodium              x86_64  7.4.26-1.el8.remi                   remi-safe   87 k

Transaction Summary
============================================================================================
Install  43 Packages

Total download size: 28 M
Installed size: 91 M
Is this ok [y/N]: y
Downloading Packages:
(1/43): jbigkit-libs-2.1-14.el8.x86_64.rpm                  401 kB/s |  54 kB     00:00
(2/43): gd-2.2.5-7.el8.x86_64.rpm
```





🌞 Analyser la conf Apache


# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf




🌞 Créer un VirtualHost qui accueillera NextCloud

```bash
[root@web conf.d]# nano nextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```






`[root@web conf.d]# systemctl reload httpd`





```bash
[root@web www]# mkdir nextcloud
[root@web www]# ls
cgi-bin  html  nextcloud
[root@web www]# cd nextcloud
[root@web nextcloud]# ls
[root@web nextcloud]# mkdir html
[root@web nextcloud]# chown root:users /var/www/nextcloud/html
[root@web nextcloud]# chmod 777 /var/www/nextcloud/html
```




🌞 Configurer PHP


```bash
[root@web nextcloud]# timedatectl
               Local time: Tue 2021-12-07 16:53:41 CET
           Universal time: Tue 2021-12-07 15:53:41 UTC
                 RTC time: Tue 2021-12-07 15:53:41
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no



date.timezone = "Europe/Paris"
```


🌞 Récupérer Nextcloud


```bash
[root@web ~]# cd /var/www/nextcloud/html/
[root@web html]# curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  2081k      0  0:01:12  0:01:12 --:--:-- 2123k
[root@web html]# ls
nextcloud-21.0.1.zip
```



🌞 Ranger la chambre



```bash
[root@web html]# unzip nextcloud-21.0.1.zip
[root@web html]# cd nextcloud
[root@web nextcloud]# ls
3rdparty  console.php  index.html  ocm-provider  remote.php  themes
apps      COPYING      index.php   ocs           resources   updater
AUTHORS   core         lib         ocs-provider  robots.txt  version.php
config    cron.php     occ         public.php    status.php
[root@web nextcloud]# chown root:users /var/www/nextcloud/html/nextcloud
[root@web nextcloud]# chmod 777 /var/www/nextcloud/html/nextcloud

[[root@web html]# rm nextcloud-21.0.1.zip
rm: remove regular file 'nextcloud-21.0.1.zip'? yes
[root@web html]# ls
nextcloud
[root@web html]# sudo mv /var/www/nextcloud/html/nextcloud/* /var/www/nextcloud/html
[root@web ~]# sudo mv /var/www/nextcloud/html/nextcloud/.htaccess /var/www/nextcloud/html
[root@web ~]# sudo mv /var/www/nextcloud/html/nextcloud/.user.ini /var/www/nextcloud/html
```



🌞 Modifiez le fichier hosts de votre PC

10.2.1.11   web.tp2.cesi


🌞 Tester l'accès à NextCloud et finaliser son install'

```bash
[root@web ~]# curl web.tp2.cesi
<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="en" data-locale="en" >
        <head
 data-requesttoken="+IyMCgUlq+fdxaOzDjJxtNUdX9H5W5LH12YaMbBIkTE=:qMngT2Fi7amcrvPEaEAc4LIuOaG4NN21m155Yfx7vnw=">
                <meta charset="utf-8">
                <title>
                Nextcloud               </title>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
                                <meta name="apple-itunes-app" content="app-id=1125420102">
                                <meta name="theme-color" content="#0082c9">
```



PARTIE2 :


🌞 Modifier la conf du serveur SSH

```bash
PS C:\Users\jacky> ssh root@10.2.1.11 -p 667 -i C:\Users\jacky\.ssh\id_rsa_pub
Warning: Identity file C:\Users\jacky\.ssh\id_rsa_pub not accessible: No such file or directory.
Enter passphrase for key 'C:\Users\jacky/.ssh/id_rsa':
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Thu Dec  9 18:14:22 2021 from 10.2.1.9
[root@localhost ~]#  //On est bien dans la VM
```



🌞 Installez et configurez fail2ban

```bash
[root@web ~]# sudo dnf install fail2ban fail2ban-firewalld -y
[root@web ~]# systemctl start fail2ban
[root@web ~]# systemctl enable fail2ban
[root@web ~]# sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
[root@web ~]# sudo nano /etc/fail2ban/jail.local

# "bantime" is the number of seconds that a host is banned.
bantime  = 10m

# A host is banned if it has generated "maxretry" during the last "findtime"
# seconds.
findtime  = 10m

# "maxretry" is the number of failures before a host get banned.
maxretry = 5

[root@web ~]# sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
[root@web ~]# sudo systemctl restart fail2ban
[root@web ~]# sudo nano /etc/fail2ban/jail.d/sshd.local

[sshd]
enabled = true
bantime = 1d
maxretry = 3

[root@web ~]# sudo systemctl restart fail2ban

```






```bash
root@10.2.1.11's password:
Permission denied, please try again.
root@10.2.1.11's password:
Permission denied, please try again.
root@10.2.1.11's password:
root@10.2.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[root@web ~]# fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 1
|  |- Total failed:     2
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.2.1.9
```







🌞 Installer NGINX


`[root@localhost ~]# yum install nginx`

🌞 Configurer NGINX comme reverse proxy




```bash
[root@localhost conf.d]# nano reverseproxy.conf

server {
    listen 80;
    server_name web.tp2.cesi;

    location /blog {
        proxy_pass http://10.2.1.11;
    }
}
[root@localhost conf.d]# systemctl restart nginx
```







🌞 Une fois en place, text !


fichier hosts :

```bash
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost


10.2.1.11    web.tp2.cesi
10.2.1.9      web.tp2.cesi
```



🌞 Générer une clé et un certificat avec la commande suivante :


```bash
[root@proxy conf.d]# openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
.....................................................................................................++++
...................................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:
Email Address []:
[root@proxy conf.d]# ls
reverseproxy.conf  server.crt  server.key
```


🌞 Allez, faut ranger la chambre

```bash
[root@proxy conf.d]# mv /etc/nginx/conf.d/server.key /etc/pki/tls/private
[root@proxy conf.d]# mv /etc/nginx/conf.d/server.crt /etc/pki/tls/certs

[root@proxy private]# mv server.key web.tp2.cesi.key
[root@proxy certs]# mv server.crt web.tp2.cesi.crt
```


🌞 Affiner la conf de NGINX

```bash
[root@proxy conf.d]# nano reverseproxy.conf
    listen 443 ssl http2;
    server_name www.example.com example.com;

ssl_certificate   /etc/pki/tls/private/web.tp2.cesi.key;
ssl_certificate_key  /etc/pki/tls/certs/web.tp2.cesi.crt;
```



🌞 Test !







PARTIE 3 :

🌞 Installez Netdata 

```bash
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-0OBSoxPLig]# curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-0OBSoxPLig/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK
```


🌞 Démarrez Netdata

```bash
[root@web ~]# sudo systemctl enable --now netdata
[root@web ~]# sudo firewall-cmd --add-port=19999/tcp --permanent
[root@web ~]# sudo firewall-cmd --reload
[root@web ~]# curl 10.2.1.11:19999
<!doctype html><html lang="en"><head><title>netdata dashboard</title><meta name="application-name" content="netdata"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta name="viewport" content="width=device-width,initial-scale=1"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><meta name="author" content="costa@tsaousis.gr"><link rel="icon" href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAP9JREFUeNpiYBgFo+A/w34gpiZ8DzWzAYgNiHGAA5UdgA73g+2gcyhgg/0DGQoweB6IBQYyFCCOGOBQwBMd/xnW09ERDtgcoEBHB+zHFQrz6egIBUasocDAcJ9OxWAhE4YQI8MDILm
```



🌞 Téléchargez Borg 

```bash
[root@localhost ~]# cd
[root@localhost ~]# curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   649  100   649    0     0   1158      0 --:--:-- --:--:-- --:--:--  1158
100 18.0M  100 18.0M    0     0  1185k      0  0:00:15  0:00:15 --:--:-- 1384k
[root@localhost ~]# sudo cp borg-linux64 /usr/local/bin/borg
[root@localhost ~]# sudo chown root:root /usr/local/bin/borg
[root@localhost ~]# sudo chmod 755 /usr/local/bin/borg
```



🌞 Jouer avec Borg


```bash
[root@localhost borg]# borg init --encryption=none /home/test/borg/backup
[root@localhost borg]# mkdir /home/test/borg/source/
[root@localhost borg]# borg create --stats --progress /home/test/borg/backup::09-12-2021 /home/test/borg/source/
------------------------------------------------------------------------------
Archive name: 09-12-2021
Archive fingerprint: b9101b0e3db2f0fdb0c9b0ae7acfeaa1fec0fba82eab376ef64a8272db683e63
Time (start): Thu, 2021-12-09 19:10:02
Time (end):   Thu, 2021-12-09 19:10:03
Duration: 0.03 seconds
Number of files: 0
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:                  649 B                620 B                620 B
All archives:                  649 B                620 B                620 B

                       Unique chunks         Total chunks
Chunk index:                       2                    2
------------------------------------------------------------------------------
[root@localhost borg]# borg create --list -v /home/test/borg/backup/::27-11-2021 /home/test/borg/source/
Creating archive at "/home/test/borg/backup/::27-11-2021"
d /home/test/borg/source
[root@localhost borg]#  borg list /home/test/borg/backup
09-12-2021                           Thu, 2021-12-09 19:10:02 [b9101b0e3db2f0fdb0c9b0ae7acfeaa1fec0fba82eab376ef64a8272db683e63]
27-11-2021                           Thu, 2021-12-09 19:11:42 [2255f6ba0ff8d0fb4906c5a654ccc9a827479ddccb65e6820d9f30cf11a02e6d]

[root@localhost borg]# borg extract -v --list backup/::09-12-2021
home/test/borg/source
```


🌞 Ecrire un script

Le script doit éxecuter ceci :

```bash
[root@localhost html]# borg create --stats --progress /srv/backup/::nextcloud_YYMMDD_HHMMSS  /var/www/nextcloud/html/next
cloud
------------------------------------------------------------------------------
Archive name: nextcloud_YYMMDD_HHMMSS
Archive fingerprint: 08888928a096a3c20d7f7fe35278fc05a156548fa77d3f30edc1ee1dc40fb718
Time (start): Thu, 2021-12-09 19:27:39
Time (end):   Thu, 2021-12-09 19:27:39
Duration: 0.02 seconds
Number of files: 0
Utilization of max. archive size: 0%
------------------------------------------------------------------------------
                       Original size      Compressed size    Deduplicated size
This archive:                  683 B                660 B                660 B
All archives:                2.66 kB              2.55 kB              2.55 kB

                       Unique chunks         Total chunks
Chunk index:                       8                    8
------------------------------------------------------------------------------
```





```bash
[root@localhost html]# nano script
borg create --stats --progress /srv/backup/::nextcloud_YYMMDD_HHMMSS  /var/www/nextcloud/html/next
cloud
[root@localhost html]# bash script
```
Et on est bon !

